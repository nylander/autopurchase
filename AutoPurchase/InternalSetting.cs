﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPurchase
{
    public class InternalSetting
    {
        public string LogFileName { get; set; }
        public string LogFileOrder { get; set; }
        public bool SkrivLoggFil { get; set; }
        public string RapportGenerator { get; set; }
        public string Rapport { get; set; }
        public string Blankett { get; set; }
        public string FilePath { get; set; }

        public string Function { get; set; }
        public string PurchasePoint { get; set; }
        public string Levels { get; set; }
        public string Quantity { get; set; }
        public string LeadTime { get; set; }
        public string Compression { get; set; }
        public string Factor { get; set; }
        public string Inventory { get; set; }
        public string Cancellation { get; set; }

        internal Settings setting { get; set; }

        public InternalSetting()
        {
            setting = Settings.Default;

            //SkrivLoggFil = setting.SkrivLoggFil;
            SkrivLoggFil = true;
            LogFileName = string.Format(@"{0}Nettobehov_{1}.txt", setting.SökvägLoggFil, DateTime.Now.ToString("yyyy-MM-dd HHmm"));
            LogFileOrder = string.Format(@"{0}Nya inköpsförslag_{1}.txt", setting.SökvägLoggFil, DateTime.Now.ToString("yyyy-MM-dd HHmm"));

            RapportGenerator = setting.DokumentGenerator;
            Rapport = setting.Rapport;
            Blankett = setting.RapportBlankett;
            FilePath = string.Format(@"{0}in258.txt", setting.SökvägOrderFil);

            Function = setting.NettobehovNTO_BTO;
            PurchasePoint = setting.NettobehovBestPunkt;
            Levels = setting.NettobehovNivå;
            Quantity = setting.NettobehovKvant;
            LeadTime = setting.NettobehovLedtid;
            Compression = setting.NettobehovKomprimering;
            Factor = setting.NettobehovBestPunktFakt;
            Inventory = setting.NettobehovLagerNr;
            Cancellation = setting.NettobehovAnnullation;
        }
    }
}
