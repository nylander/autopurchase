﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AutoPurchase
{
    class Program
    {
        public static Garp.Application GarpApp;
        private static Logininfo login = null;
        private static InternalSetting setting = null;
        public static bool OK;
        static void Main(string[] args)
        {
            try
            {
                login = new Logininfo();
                setting = new InternalSetting();

                GarpLogin();
                DeletePurchaseSuggestions();
                OK = NetMaterialRequirements();
                if (OK) { OK = RunGarpReport(setting.RapportGenerator, setting.Rapport, setting.FilePath, setting.Blankett); }
                if (OK) { CreatePurchase(); }
            }
            catch (Exception e)
            {
                Console.Write("Error " + e);
                Logfil("Fel " + e.Message);

            }
        }

        private static void DeletePurchaseSuggestions()
        {
            string text = "Raderar order:" + Environment.NewLine;
            bool findorder = false;
            Garp.ITable Purchase = GarpApp.Tables.Item("IGA");
            Garp.ITabFilter filter = Purchase.Filter;
            filter.AddField("OTY");
            filter.AddField("X1F");
            filter.AddConst("0");
            filter.AddConst("X");
            filter.Expression = "1=3&2=4";
            filter.Active = true;

            Purchase.First();
            while (Purchase.Eof != true)
            {
                text += Purchase.Fields.Item("ONR").Value + Environment.NewLine;
                Purchase.Delete();
                findorder = true;
                Purchase.Next();
            }
            if (findorder)
            {
                Logfil(text + Environment.NewLine);
            }
            else
            {
                text = "Hittade ingen order att radera" + Environment.NewLine;
                Logfil(text);
            }

        }

        private static bool NetMaterialRequirements()
        {
            try
            {
                Logfil("Startar materialplanering ");

                Garp.IMPSNetMaterialRequirements requirement = GarpApp.MPSNetMaterialRequirements;
                requirement.Delete();
                //requirement.ItemNoFrom = "556115";
                //requirement.ItemNoTo = "556115";
                requirement.CalcFunc = setting.Function;
                requirement.PurchasePoint = setting.PurchasePoint;
                requirement.Levels = setting.Levels;
                requirement.Quantity = setting.Quantity;
                requirement.LeadTime = setting.LeadTime;
                requirement.Compression = setting.Compression;
                requirement.PurchasePointFactor = setting.Factor;
                requirement.InventoryNo = setting.Inventory;
                requirement.CreateCancellationSuggestion = setting.Cancellation;
                requirement.Run();
                Logfil("- klar, status: " + requirement.Status + Environment.NewLine);
                return true;
            }
            catch (Exception e)
            {
                Logfil("Fel vid materialplanering!" + Environment.NewLine + e.Message);
                return false;
            }
        }

        private static bool RunGarpReport(string Generator, string Rapport, string FileName, string Blankett, string IndexFrom = "", string IndexTom = "ö")
        {
            try
            {
                Logfil("Startar Rapportutskrift ");
                Garp.IReport report = GarpApp.ReportGenerators.Item(Generator).Reports.Item(Rapport);
                if (report == null)
                {
                    Logfil("Hittade inte rapport " + Generator + "-" + Rapport + Environment.NewLine);
                    return false;
                }

                report.RangeFrom = IndexFrom;
                report.RangeTo = IndexTom;
                report.Form = Blankett;
                report.Medium = "D";
                report.FilePath = FileName;
                report.Run();
                report.Wait();

                Logfil("- klar" + Environment.NewLine);
                return true;
            }
            catch (Exception e)
            {
                Logfil("Fel vid rapportutskrift!" + Environment.NewLine + e.Message);
                return false;
            }
        }

        private static void CreatePurchase()
        {
            try
            {
                Logfil("Skapar inköp ");
                Garp.IEDI purchase = GarpApp.EDI;
                purchase.FilePath = setting.FilePath;
                purchase.LoggFile = setting.LogFileOrder;
                purchase.RemoveFile = true;
                purchase.CreatePurchaseOrders();
                Logfil(" - klar. Logfil inköp: " + setting.LogFileOrder + Environment.NewLine);
            }
            catch (Exception e)
            {
                Logfil("Fel vid skapa inköpsorder!" + Environment.NewLine + e.Message);
            }
        }

        private static void GarpLogin()
        {
            try
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GarpApp = new Garp.Application();

                //Loggar in i Garp och sätter bolag, användare och lösen
                Logfil("Loggar in i Garp. ");
                if (Settings.Default.AnvändKonfiguration == true)
                {
                    GarpApp.ChangeConfig(login.GarpConfig);
                }
                GarpApp.Login(login.Usr, login.Password);
                //GarpApp.SetBolag(login.Bolag);

                if (GarpApp.User == null)
                {
                    GarpApp = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Logfil("Kan ej logga in i Garp!" + Environment.NewLine);

                    Environment.Exit(0);
                }

                Logfil("Inloggad som " + GarpApp.User + ", terminal " + GarpApp.Terminal + " och bolag " + GarpApp.Bolag + Environment.NewLine);
            }
            catch (Exception e)
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Logfil("Kan ej logga in i Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message);

                Environment.Exit(0);
            }
        }

        private static void EndGarp()
        {
            //Stäng Garp
            GarpApp = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Logfil("Stänger Garp");
        }

        private static void Logfil(string text)
        {
            Console.Write(text);
            if (setting.SkrivLoggFil)
            {
                File.AppendAllText(setting.LogFileName, text);
            }
        }

    }
}
